#pragma once


#include "xor2.h"
#include "und2.h"
#include "baustein.h"
#include "schaltung.h"

class Halbaddierer : public Schaltung
{
public:
	Halbaddierer(Schnittstelle* e0, Schnittstelle* e1, Schnittstelle* a0, Schnittstelle* a1)
	{
		addEingang(e0);
		addEingang(e1);
		addAusgang(a0);
		addAusgang(a1);

		Xor2* p_Xor2 = new Xor2(e0,e1,a0) ;
		baustein.push_back(p_Xor2);
		
		Und2* p_Und2 = new Und2(e0, e1, a1);
		baustein.push_back(p_Und2);

		name = "Halbaddierer";
	}

	void update()
	{
		baustein[0]->update();
		baustein[1]->update();
	}
};