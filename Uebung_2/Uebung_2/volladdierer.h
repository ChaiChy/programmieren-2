#pragma once
#include "Oder2.h"
#include "schnittstelle.h"
#include "baustein.h"
#include "schaltung.h"
#include "halbaddierer.h"

class Volladdierer : public Schaltung
{
public:
	Volladdierer(Schnittstelle* e0, Schnittstelle* e1, Schnittstelle* e2, Schnittstelle* a0, Schnittstelle* a1)
	{
		name = "Volladdierer"; 

		addEingang(e0);
		addEingang(e1);
		addEingang(e2);
		addAusgang(a0);
		addAusgang(a1);

		// Erzeuge die internen Schnittstellen
		Schnittstelle* i0 = new Schnittstelle; 
		Schnittstelle* i1 = new Schnittstelle;
		Schnittstelle* i2 = new Schnittstelle;

		// Speichere die Schnittstellen im intern Vector
		intern.push_back(i0);
		intern.push_back(i1);
		intern.push_back(i2);
		 
		// Erzeuge die Halbaddierer
		Halbaddierer* HA0 = new Halbaddierer(e0, e1, i1, i0 );
		Halbaddierer* HA1 = new Halbaddierer(e2, i1, a0, i2 );

		baustein.push_back(HA0);
		baustein.push_back(HA1);

		// Erzeuge den Oder2 Baustein
		Oder2* Oder2_3 = new Oder2(i0, i2, a1);

		baustein.push_back(Oder2_3);
	}

	void update()
	{
		baustein[0]->update();	// HA0->Update()
		baustein[1]->update();	// HA1->Update()
		baustein[2]->update();	// Oder2_3->Update()
	}
};