#pragma once

#include "baustein.h"
#include "schnittstelle.h"

class Oder2 : public Baustein
{
	public:

	Oder2() {};
	Oder2(Schnittstelle* e0, Schnittstelle* e1, Schnittstelle* a0)
	{
		name = "Oder2";
		addEingang(e0);
		addEingang(e1);
		addAusgang(a0);
	}

	void update()
	{
		// Check nach undefinierten eing�ngen
		if (eingang[0]->getPegel() == Schnittstelle::UNDEFINED || eingang[1]->getPegel() == Schnittstelle::UNDEFINED)
		{
			ausgang[0]->setPegel(Schnittstelle::UNDEFINED);
			return;
		}
		// e0| e1| a0
		// 0 | 0 | 0
		else if (eingang[0]->getPegel() == Schnittstelle::LOW && eingang[1]->getPegel() == Schnittstelle::LOW)
		{
			ausgang[0]->setPegel(Schnittstelle::LOW);
			return;
		}
		// e0| e1| a0
		// 0 | 1 | 1
		// 1 | 0 | 1
		// 1 | 1 | 1
		else
		{
			ausgang[0]->setPegel(Schnittstelle::HIGH);
			return;
		}

	}
};