#pragma once


class Schnittstelle
{
private: 
	short pegel = LOW;

public: 

	const static short HIGH = 1;
	const static short LOW = 0;
	const static short UNDEFINED = -1;
	
	short getPegel()
	{
		return this->pegel;
	}
		
	bool setPegel(short pegel)
	{
		if (pegel != HIGH && pegel != LOW)
		{
			this->pegel = UNDEFINED;
			return false;
		}
		else
		{
			this->pegel = pegel;
			return true;
		}
	}
};