#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "schnittstelle.h"

class Baustein
{
protected:
	std::string name = "Baustein";
	std::vector<Schnittstelle*> eingang;
	std::vector<Schnittstelle*> ausgang;

public:
	friend void test2(Baustein* b);
	friend void testVA2(Baustein* b);

	void addEingang(Schnittstelle* schnittstelle)
	{
		eingang.push_back(schnittstelle);
	}

	void addAusgang(Schnittstelle* schnittstelle)
	{
		ausgang.push_back(schnittstelle);
	}

	virtual void print()
	{
		//Ausgabe des Names des Bausteins
		std::cout << "Baustein: " << name << std::endl;
		
		//Ausgange der Eing�nge
		for (int i = 0; i < eingang.size(); i++) std::cout << "Eingang " << i << ": Pegel = " << eingang[i]->getPegel() << std::endl;
	
		//Ausgabe der Ausg�nge
		for (int i = 0; i < ausgang.size(); i++) std::cout << "Ausgang " << i << ": Pegel = " << ausgang[i]->getPegel() << std::endl;
	}

	virtual void update() = 0;
};

/*
Warum werden Zeiger f�r die Schnittstellen verwendet? 
- Da der Datentyp Vector am st�ck im Speicher abgelegt wird es so zu problemen bei mehr Eing�ngen kommen kann da nicht so viel Speicher
- am st�ck vorhanden ist. 

Warum wurden die Attribute als protected gekennzeichnet?
- Um eine geordetes/systematisches abspeichern (richtige reihenfolge) zu gew�hrleisten

Der schreibende Zugriff auf das Attribut name ist nirgends m�glich. Warum wird dennoch auf eine set-Funktion f�r name verzichtet?
- Da bei der Vererbung die Kind-klasse immernoch zugriff auf den das Name-Attribut hat und diese beschreiben kann.

Was bewirkt das Schl�sselwort virtual?
- Das bei laufzeit des Programmes zuerst in den Kind-Klassen nach einem Identischen Funktion gesucht. Falls eine vorhanden ist wird
- diese anstelle der urspr�nglichen aufgerufen. 

Welche Motivation k�nnte der Programmierer haben die Funktion als virtual zu deklarieren? (Print())
- Falls in den Kind-Klassen zus�tzliche Attribute aufgenommen werden die ebenfalls in der Print() funktion ausgegeben werden sollen
- kann der Programmierer diese in den Kind-Klassen in einer neuen Print() funktion hinzuf�gen.  
*/