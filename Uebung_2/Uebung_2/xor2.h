#pragma once

#include "baustein.h"
#include "schnittstelle.h"

class Xor2 : public Baustein
{
public:

	Xor2() {};
	Xor2(Schnittstelle* e0, Schnittstelle* e1, Schnittstelle* a0)
	{
		name = "Xor2";
		addEingang(e0);
		addEingang(e1);
		addAusgang(a0);
	}

	void update()
	{
		// Check nach undefinierten eing�ngen
		if (eingang[0]->getPegel() == Schnittstelle::UNDEFINED || eingang[1]->getPegel() == Schnittstelle::UNDEFINED)
		{
			ausgang[0]->setPegel(Schnittstelle::UNDEFINED);
			return;
		}
		// e0| e1| a0
		// 0 | 1 | 1
		else if (eingang[0]->getPegel() == Schnittstelle::LOW && eingang[1]->getPegel() == Schnittstelle::HIGH)
		{
			ausgang[0]->setPegel(Schnittstelle::HIGH);
			return;
		}
		// e0| e1| a0
		// 1 | 0 | 1
		else if (eingang[0]->getPegel() == Schnittstelle::HIGH && eingang[1]->getPegel() == Schnittstelle::LOW)
		{
			ausgang[0]->setPegel(Schnittstelle::HIGH);
			return;
		}
		// e0| e1| a0
		// 1 | 1 | 0
		// 0 | 0 | 0
		else
		{
			ausgang[0]->setPegel(Schnittstelle::LOW);
			return;
		}

	}
};

/*
K�nnte man die Klasse Xor2 auch durch eine Schaltung realisieren?
- Ja einmal aus einem OR-, NAND- und AND-Gatter oder mit 4 NAND-Gatter. Jedoch fehlen f�hr beide das Bauteil NAND 
- was sich einfach mit einem zus�tzlichen Negations Bauteil realisieren lassen w�rde. 
*/