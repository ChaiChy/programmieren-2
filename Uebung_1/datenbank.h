#ifndef DATENBANK_H_
#define DATENBANK_H_

#define DB_LIMIT 10
#include "kennzeichen.h"
#include <string>

namespace db
{
	struct datenbank
	{
		std::string name;
		kfz::kennzeichen* schilder[DB_LIMIT];
		int eintraege = 0;
	};

	typedef struct datenbank datenbank;

	  /////////////////////////////////
	 //////////// Aufgabe 2 //////////
	/////////////////////////////////

	bool einfuegen(datenbank* db, kfz::kennzeichen* schild)
	{
		if (db->eintraege > DB_LIMIT ) return false;

		db->schilder[db->eintraege] = schild;

		db->eintraege++;

		return true;
	}

	std::string ausgabe(const datenbank& db)
	{
		std::string temp;
		temp += "Datenbank: ";
		temp += db.name;
		temp += "\n---------------\n";

		for (int i = 0; i < db.eintraege; i++)
		{
			temp += kfz::ausgabe(*(db.schilder[i]));
			temp += "\n";
		}

		return temp;
	}

}

#endif /* DATENBANK_H_ */
