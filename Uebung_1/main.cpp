#include <iostream>
#include <string>
#include "kennzeichen.h"
#include "datenbank.h"


  /////////////////////////////////
 //////////// Aufgabe 2 //////////
/////////////////////////////////

// Teilaufgabe a)
//---------------

void schnapszahlTest()
{

	std::cout << "SchnapszahlTest:" << std::endl;
	std::cout << "===================" << std::endl;

	kfz::kennzeichen temp;
	kfz::kennzeichen* test = &temp;

	test->buchstaben = "LL";
	test->ort = "VB";
	test->zahl = 444;

	std::cout << test->ort << "-" << test->buchstaben << "-" << test->zahl << " hat ";

	if (kfz::istSchnapszahl(test))
	{
		std::cout << "eine Schnapszahl." << std::endl;
	}
	else
	{
		std::cout << "keine Schnapszahl." << std::endl;
	}
}

// Teilaufgabe b)
//---------------

void einlesenTest()
{
	std::cout << "EinleseTest" << std::endl;
	std::cout << "===================" << std::endl;

	kfz::kennzeichen* test = kfz::einlesen();
	std::cout << kfz::ausgabe(*test) << std::endl;
	kfz::schildTest(test);
	delete test;
}

  /////////////////////////////////
 //////////// Aufgabe 4 //////////
/////////////////////////////////

void datenbankTest()
{
	std::cout << "DatenbankTest:" << std::endl;
	std::cout << "===================" << std::endl;

	struct db::datenbank temp;
	db::datenbank* test = &temp;
	test->name = "Landsberg";

	db::einfuegen(test, kfz::einlesen());
	db::einfuegen(test, kfz::einlesen());

	std::cout << db::ausgabe(*test) << std::endl;
}


int main()
{
	schnapszahlTest();
	einlesenTest();
	datenbankTest();

	return 0;
}
